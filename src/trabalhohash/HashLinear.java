/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package trabalhohash;
import javax.swing.JOptionPane;
/**
 * @data de início 03/04/2017 
 * @data de término 05/04/2017 
 * @author MILAD ROGHANIAN RA160985
 */

///######## CLASSE HASH LINEAR CONTÉM TODAS AS FUNÇÕES PARA MANIPULAR O VETOR DE HASH DA FORMA LINEAR ###########
public class HashLinear {
    private int TAM;
    private Integer vetor[]; //Separa-se uma posição na memória para um vetor de inteiros (como classe Integer para poder verificar o nulo)
    
    public void inicializaVetor()
    {
        String aux;
        //Pergunta-se ao usuário quantos valores deseja inserir para se ter uma noção do tamanho do vetor
        aux = JOptionPane.showInputDialog("Quantos valores deseja inserir no Vetor Hash?");
        if (aux!=null){ //Se o usuário clicar em ok vai-se retornar o valor digitado para a String
            TAM = Integer.parseInt(aux);
        } else { //Se não (clicando no X da janela ou em Cancelar) a String receberá nullo e fecha-se o programa
            System.exit(0);
        }
        this.vetor = new Integer[this.TAM]; //Cria-se o vetor de inteiros
    }
    
    //### INSERE ###
    public void insereValor (int valor)
    {
        //FÓRMULA -> h(k)=k*k mod TAM
        int x = valor*valor%this.TAM; //Calcula-se a posição a ser inserida
        int i=0; //O "i" serve para rodamos em todas as posições do vetor, sendo comparada a TAM (abaixo)
        while (this.vetor[x]!=null && i!=this.TAM-1){ //Se a posição do vetor não for nula e não tivermos rodado todo o vetor
            if (x!=this.TAM-1){ //Se X for menor que o tamanho do vetor
                x++; //Busca-se na próxima posição
            } else { //Se não reinicia-se a busca de posição vazia
                x=0;
            }
            i++;
        }
        if (i!=this.TAM-1){ //Se não tivermos rodado o vetor todo isso indica que achamos posição vazia
            this.vetor[x]=valor; 
        } else { //Senão
            //Essa parte pergunta ao usuário se ele deseja sobrescrever alguma posição do vetor pois ele está cheio
            Object[] principal = {"SIM", "NÃO" };
            int op;
            op = JOptionPane.showOptionDialog(null, "ERRO! Vetor cheio, deseja sobrescrever?", "VETOR CHEIO", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, principal, principal[0]);
            if (op==0){ //Se sim ele sobrescreverá, Senão não faz nada
                this.vetor[x]=valor;
            }
        }
    }
    
    //### BUSCA ###
    public void buscaValor (int valor)
    {
        int x = valor*valor%this.TAM; 
        int i;
        //Os mesmos cálculos de posição são feitos para se procurar a posição passada como parâmetro
        for (i=0; i<this.TAM; i++)
        {
            if (this.vetor[x]==valor){
                i=this.TAM+5;
            } else if (x!=this.TAM-1) {
                x++;
            } else {
                x=0;
            }   
        }
        if (this.vetor[x]==valor){ //Se for compatível com o parâmetro
            JOptionPane.showMessageDialog(null,"Valor "+valor+" EXISTE na posição "+(x+1)+" da tabela Hash");
        } else {
            JOptionPane.showMessageDialog(null,"Valor "+valor+" NÃO existe na tabela Hash");
        }
    }
    
    //### IMPRIME ###
    public String imprimeHash()
    {
        String imprimir = new String(); //Cria-se uma variável imprimir da classe String
        for (int i=0; i<this.TAM; i++) //Percorre-se o vetor
        {
            if (this.vetor[i]!=null){ //Se a posição do vetor for diferente de nulo
                imprimir = imprimir+"POS "+(i+1)+" --> "+this.vetor[i]+"\n"; //A String Recebe o valor
            } else { //Senão 
                imprimir = imprimir+"POS "+(i+1)+"\n"; //Somente escreve-se a posição vazia
            }
        } 
        return imprimir; //Retorna-se a String com a impressão
    }

    /**
     * @return the TAM
     */
    public int getTAM() {
        return TAM;
    }
}
