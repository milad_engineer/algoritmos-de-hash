/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package trabalhohash;
import javax.swing.JOptionPane;
/**
 * @data de início 03/04/2017 
 * @data de término 05/04/2017 
 * @author MILAD ROGHANIAN RA160985
 */

///################# CLASSE CONTROLE CONTÉM FUNÇÕES PARA GERENCIAR AS CLASSES HASH #################
public class Controle {
    
    private boolean escolha;
    private HashEncadeado he; //Separa espaço na memória para Hash Encadeado
    private HashLinear hl; //Separa espaço na memória para Hash Linear
    public void escolhaHash ()
    {
        //Pergunta ao usuário que tipo de Hash deseja executar
        Object[] principal = {"Hash Encadeado", "Hash Linear" }; 
        int op;
        op = JOptionPane.showOptionDialog(null, "Escolha o modo Hashing", "HASHING", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, principal, principal[0]);
        //TRUE para Hash Encadeado e FALSE para Hash Linear
        if (op==0){ //Se op=0 o usuário clicou em Hash Encadeado
            this.escolha = true;
            this.he = new HashEncadeado();
            he.inicializaVetor();
        } else if (op==1){ //Se op=1 o usuário clicou em Hash Encadeado
            this.escolha = false;
            this.hl = new HashLinear(); //Cria-se a posição do Hash Linear
            hl.inicializaVetor(); //Inicializa-se o Array list
        } else { //Se op não é igual a 1 ou 0 o usuário fechou a janela
            System.exit(0); //Então encerra-se o programa
        }
    }
            
    //### INSERE ###
    public void insere(int valor) 
    {
        if (this.escolha){ //Dependendo de qual Hash o usuário escolheu
            he.insereValor(valor); //Manda o valor para o insere da classe HashEncadeado
        } else {
            hl.insereValor(valor); //Manda o valor para o insere da classe HashLinear
        }
    }
    
    //### BUSCA ###
    public void busca(int valor)
    {
        if (this.escolha){ //Dependendo de qual Hash o usuário escolheu
            he.buscaValor(valor); //Manda o valor para o busca da classe HashEncadeado
        } else {
            hl.buscaValor(valor); //Manda o valor para o busca da classe HashLinear
        }
    }
    
    //### IMPRIME ###
    public String imprime ()
    {
        String imprimir;
        if (this.escolha){ //Dependendo de qual Hash o usuário escolheu
            imprimir = he.imprimeHash(); //Manda o valor para o imprime da classe HashEncadeado
        } else {
            imprimir = hl.imprimeHash(); //Manda o valor para o imprime da classe HashLinear
        }
        return imprimir; //retorna a String recebida com a impressão do vetor
    }
    
    //### RETORNA TAMANHO DO VETOR ###
    public String tamanhoVetor()
    {
        String tam = new String();
        if (this.escolha){ //Dependendo de qual Hash o usuário escolheu
            tam = "TAM DO VETOR = "+he.getTAM(); //Pega o tamanho 
        } else {
            tam = "TAM DO VETOR = "+hl.getTAM(); //Pega o tamanho 
        }
        return tam; //Retorna a String tamanho
    }

    /**
     * @return the escolha
     */
    public boolean isEscolha() {
        return escolha;
    }
}
