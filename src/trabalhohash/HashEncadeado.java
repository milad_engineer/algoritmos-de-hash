/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package trabalhohash;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 * @data de início 03/04/2017 
 * @data de término 05/04/2017 
 * @author MILAD ROGHANIAN RA160985
 */

///######## CLASSE HASH ENCADEADO CONTÉM TODAS AS FUNÇÕES PARA MANIPULAR O VETOR DE HASH DA FORMA ENCADEADA ###########
public class HashEncadeado {
    private int TAM;
    private ArrayList<Integer> vetor[]; //Separa-se uma posição na memória para um vetor de ArrayList do tipo Inteiro
    
    public void inicializaVetor()
    {
        String aux;
        //Pergunta-se ao usuário quantos valores deseja inserir para se ter uma noção do tamanho do vetor
        aux = JOptionPane.showInputDialog("Quantos valores deseja inserir no Vetor Hash?");
        if (aux!=null){//Se o usuário clicar em ok vai-se retornar o valor digitado para a String
            TAM = Integer.parseInt(aux); 
        } else { //Se não (clicando no X da janela ou em Cancelar) a String receberá nullo e fecha-se o programa
            System.exit(0);
        }
        this.vetor = new ArrayList[this.TAM]; // Cria-se o vetor de Array
        for (int i=0; i<this.TAM; i++) //Percorre o vetor
        {
            this.vetor[i] = new ArrayList<Integer>();//Inicia-se cada posição do vetor como uma ArrayList
        }
    }
    
    //### INSERE ###
    public void insereValor (int valor)
    {
        //FÓRMULA -> h(k) = k*mod TAM
        int x = valor%this.TAM; //Calcula-se a posição a ser inserida
        this.vetor[x].add(valor); //Adiciona-se o valor na posição calculada do vetor na última posição da ArrayList
    }
    
    //### BUSCA ###
    public void buscaValor (int valor)
    {
        int x = valor%this.TAM; //Calcula-se a posição a ser inserida
        boolean verifica=false;
        for (int i=0;i<this.vetor[x].size();i++) // vai até a última posição do vetor (lista.size())
	{
            if (this.vetor[x].get(i)==valor){ //pesquisa da posição i o valor e compara com o valor passado por parâmetro
                verifica=true; //Se existir o valor o booleano recebe verdadeiro pois cairá no IF
            }
        }
        if (verifica){ //Se o valor existir
            JOptionPane.showMessageDialog(null,"Valor "+valor+" EXISTE na posição "+(x+1)+" da tabela Hash");
        } else {
            JOptionPane.showMessageDialog(null,"Valor "+valor+" NÃO existe na tabela Hash");
        }
    }
    
    //### IMPRIME ###
    public String imprimeHash ()
    {
        String imprimir = new String(); //Cria-se uma variável imprimir da classe String
        for (int i=0; i<getTAM(); i++) //Percorre-se o vetor
        {
            imprimir = imprimir+"POS "+(i+1)+"--> "; //a String recebe as posições do vetor
            for (int j=0;j<this.vetor[i].size();j++) //Entra-se em cada elemento da ArrayList
            {
                imprimir = imprimir+" "+this.vetor[i].get(j); //A String recebe os elementro existentes na Array
            }
            imprimir = imprimir+"\n"; //Pula-se linha para a próxima posição 
        }
        return imprimir; //Retorna-se a String com a impressão
    }

    /**
     * @return the TAM
     */
    public int getTAM() {
        return TAM;
    }
}
